/**Activity instructions:*
1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
- The course name should be in the card title.
- The description and price should be in the card body.

2. Render the CourseCard component in the Home page.

3. Push to git with the commit message of Add activity code - S45.

4. Add the link in Boodle.
*/

/**Activity Instructions:*
1. Create a seats state in the CourseCard component and set the initial value to 30.

2. For every enrollment, deduct one to the seats.

3. If the seats reaches zero do the following:
- Do not add to the count.
- Do not deduct to the seats.

4. Push to git with the commit message of Add activity code - S46.

5. Add the link in Boodle.*/

// import state hook from react
// import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
	// Check to see if the data was successfully passed
	// console.log(props);
	// console.log(typeof props);
	const {_id, name, description, price } = courseProp;

	//Use state hook in this component to be able to store its state
	//States are used to keep track of information related to individual components
	/* Syntax:
		const [getter, setter] = useState(initialGetterValue);
	 */
/*	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [isOpen, setIsOpen]= useState(false);

	console.log(useState(30));*/

	// Function to keep track of the enrollees for a course
	// function enroll(){
	// 	setCount(count + 1);
	// 	console.log('Enrollees: ' + count);
	// }

	// function enroll(){
	// 	if (seats > 0){
	// 	setCount(count + 1);
	// 	console.log('Enrollees: ' + count)
	// 	setSeats(seats - 1);
	// 	console.log('Seats: ' + seats)
	// 	}else{
	// 	alert("No more seats available!");
	// 	document.getElementById("enrollBtn").disabled = false;
	// 	}
	// }

/*	function enroll(){
		setCount(count + 1);
		console.log('Enrollees: ' + count);		
		setSeats(seats - 1);
		console.log('Seats: ' + seats);
	}*/

	// Define a useEffect hook to have the CourseCard component perform a certain task after every DOM update
/*	useEffect(() => {
		if(seats === 0){
			setIsOpen(true);
		}
	}, [seats])*/

	return (
		<Card className="mb-2">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
} 