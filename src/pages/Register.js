import { useState,useEffect, useContext } from 'react';
import { Redirect, useHistory } from 'react-router-dom'; 
import { Form, Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext)

	const history = useHistory();

	// State hooks to store the values of the input fields
	const [firstName, setFname] = useState('');
	const [lastName, setLname] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// State to determine wether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(age);
	console.log(gender);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);


	// Function to simulate user registration
	// function registerUser(e) {
		// Prevents page redirection via form submission
		// e.preventDefault();

		// Clear input fields
		// setFname('');
		// setLname('');
		// setAge('');
		// setEmail('');
		// setMobileNo('');
		// setPassword1('');
		// setPassword2('');

	// 	alert('Thank you for registering!')
	// }

const registerUser = (e) => {

		e.preventDefault();

		fetch('http://localhost:4000/api/users/checkEmail', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Please provide a different email."
				})
			}
			else{
				fetch('http://localhost:4000/api/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						age: age,
						gender: gender,
						email: email,
						password: password1,
						mobileNo: mobileNo
						
					})
					
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);
	
					if(data === true){
						Swal.fire({
							title: "Successfully registered",
							icon: "success",
							text: "You have successfully registered an account."
						})
						setFname('');
						setLname('');
						setAge('');
						setGender('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						history.push("/login")
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		// Validation to enable the submit button when all fields are populated and both passwords match
		if ((email !== '' &&
				password1 !== '' &&
				password2 !== '' &&
				firstName !== '' &&
				lastName !== '' &&
				age !== '' &&
				mobileNo !== '') &&
			(mobileNo.length === 11) &&
			(password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, age, mobileNo])

	return (
		(user.email === null) ?
		<Redirect to="./courses" />
		:

		<Container className="mb-5 mt-3">
			<h1>Register</h1>

			{/* Firstname */}
			  <Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter First Name" 
					value= {firstName}
					onChange= { e => setFname(e.target.value) }
					required 
				/>
			  </Form.Group>

			{/* Lastname */}
				<Form.Group className="mb-3" controlId="userEmail">
				  <Form.Label>Last Name</Form.Label>
				  <Form.Control 
					type="text" 
					placeholder="Enter Last Name" 
					value= {lastName}
					onChange= { e => setLname(e.target.value) }
					required 
				  />
				</Form.Group>

			{/* Age */}
				<Form.Group className="mb-3" controlId="userEmail">
				  <Form.Label>Age</Form.Label>
				  <Form.Control 
					type="number" 
					placeholder="Enter Age" 
					value= {age}
					onChange= { e => setAge(e.target.value) }
					required 
				  />
				</Form.Group>

			{/* Gender */}
				<Form.Group className="mb-3" controlId="userEmail">
				  <Form.Label>Gender</Form.Label>
				  <Form.Select
						type="text" 
						placeholder="Select Gender" 
						value = {gender}
						onChange = { e => setGender(e.target.value)}
						required>

						<option value="">Select Gender</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
						<option value="Other">Other</option>
						</Form.Select>
				  </Form.Group>

			{/* Mobile Number */}
				<Form.Group className="mb-3" controlId="userEmail">
				  <Form.Label>Mobile Number</Form.Label>
				  <Form.Control 
					type="text" 
					placeholder="Enter Mobile Number" 
					value= {mobileNo}
					onChange= { e => setMobileNo(e.target.value) }
					required 
				  />
				</Form.Group>

			{/* Email */}
			<Form onSubmit={(e) => registerUser(e)}>
			  <Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email" 
					value= {email}
					onChange= { e => setEmail(e.target.value) }
					required 
				/>
				<Form.Text className="text-muted">
				  We'll never share your email with anyone else.
				</Form.Text>
			  </Form.Group>

			{/* Password */}
			  <Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password" 
					value= {password1}
					onChange= { e => setPassword1(e.target.value)}
					required 
				/>
			  </Form.Group>

			{/* Verify Password */}
			  <Form.Group className="mb-3" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Verify Password" 
					value= {password2}
					onChange= { e => setPassword2(e.target.value)}
					required 
				/>
			  </Form.Group>
			  
			 {/* Conditionally rnder submit button based on isActive state*/}
			  { isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
					  Submit
					</Button>
				:
					<Button variant="primary" type="submit" id="submitBtn" disabled>
					  Submit
					</Button>
			   }
			</Form>
		</Container>
	)
}
/*
*Activity instructions:*
1. If the user goes to /register route (Registration page) when a user is already logged in, redirect the user to the /courses route (Courses page).

2. Push to git with the commit message of Add activity code - S49.

3. Add the link in Boodle.
*/

// *Activity instructions:*
// 1. Refactor the Register page to include additional form inputs for the user's first name, last name, age, and mobile number.

// 2. Refactor the useEffect hook for the form validation to include the user’s first name, last name and mobile number with at least 11 digits.

// 3. Make the register feature work that will prevent the user from registering if the email already exists.

// 4. Redirect the user to the Login page upon successful registration.

// 5. Push to git with the commit message of Add activity code - S50.

// 6. Add the link in Boodle.