/* *Activity Instructions:*
1. Create a route which will be accessed when a user enters an undefined route and display the Error page.
- Recall and listen to your instructor’s explanation on how the Route component works.

2. Refactor the Banner component to be useable for the Error page and the Home page.

3. Push to git with the commit message of Add activity code - S48.*/

// import React from 'react';
// import { Link } from 'react-router-dom';

// const Error = () => (
//   <div>
//     <h1>404 - Not Found!</h1>
//     <Link to="/">Go Home</Link>
//   </div>
// );

// export default Error;

import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title: "404 - Not found",
		content: "The page you are looking for isn't a registered route",
		destination: "/",
		label: "Go Home"
	}

	return (
		<Banner data={data}/>
	)
}