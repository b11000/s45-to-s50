
const courseData = [
	{
		id : "wdc001",
		name : "PHP -  Laravel",
		description : "Lorem ipsum dolor quis laboris labore eu cupidatat ullamco ut adipisicing esse pariatur duis irure sit commodo enim velit aute. Ad id magna in ad e iusmod aliqua officia eiusmod sunt quis dolor ut consectetur nulla velit. Ea sunt ex consectetur sunt veniam eu sint laboris deserunt est.",
		price: 45000,
		onOffer : true,
	},
	{
		id : "wdc002",
		name : "Python - Django",
		description : "Lorem ipsum dolor quis laboris labore eu cupidatat ullamco ut adipisicing esse pariatur duis irure sit commodo enim velit aute. Ad id magna in ad eiusmod aliqua officia eiusmod sunt quis dolor ut consectetur nulla velit. Ea sunt ex consectetur sunt veniam eu sint laboris deserunt est.",
		price: 50000,
		onOffer : true,
	},
	{
		id : "wdc003",
		name : "Java -Springboot",
		description : "Lorem ipsum dolor quis laboris labore eu cupidatat ullamco ut adipisicing esse pariatur duis irure sit commodo enim velit aute. Ad id magna in ad eiusmod aliqua officia eiusmod sunt quis dolor ut consectetur nulla velit. Ea sunt ex consectetur sunt veniam eu sint laboris deserunt est.",
		price: 55000,
		onOffer : true,
	},
	{
		id : "wdc004",
		name : "HTML Introduction",
		description : "Lorem ipsum dolor quis laboris labore eu cupidatat ullamco ut adipisicing esse pariatur duis irure sit commodo enim velit aute. Ad id magna in ad eiusmod aliqua officia eiusmod sunt quis dolor ut consectetur nulla velit. Ea sunt ex consectetur sunt veniam eu sint laboris deserunt est.",
		price: 60000,
		onOffer : true,
	}
];

export default courseData;